#!/bin/bash
reset

#Simple Neighborhoods
#testes=( TT_M TT_S TT_KS TT_KE TT_TM TT_RM TT_LM TT_RS TT_MWD )

#Combined Neighborhoods
#testes=( TT_LM_RM TT_LM_TM TT_RM_TM TT_LM_RM_TM TT_LM_KE TT_LM_MWD TT_LM_RSM )

#Other Combined Neighborhoods
#testes=( TT_MS TT_MS_KE TT_MS_MWD TT_MS_RSM )

#All Neighborhoods
testes=( TT_M TT_S TT_KS TT_KE TT_TM TT_RM TT_LM TT_RS TT_MWD TT_LM_RM TT_LM_TM TT_RM_TM TT_LM_RM_TM TT_LM_KE TT_LM_MWD TT_LM_RSM TT_MS TT_MS_KE TT_MS_MWD TT_MS_RSM )

#Informações colhidas nos testes:
#Seed
#fo da construção
#tempo da construção
#fo da busca local
#tempo da busca local
#número de iteracoes da busca local
#número de movimentos realizados
#número de movimentos tentados
#número de movimentos tentados de cada tipo
#número total de iterações GRASP
#tempo total


cd src
mkdir -p bin
make
cd ../src

mkdir -p ../results
mkdir -p ../tests
mkdir -p ../tests/summaryperinstance

seeds=( 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 )
instances=( comp01 comp02 comp03 comp04 comp05 comp06 comp07 comp08 comp09 comp10 comp11 comp12 comp13 comp14 comp15 comp16 comp17 comp18 comp19 comp20 comp21 )


title="$instance"
for teste in ${testes[@]} ; do
	echo "testing $teste..."
	#Cria o diretório onde vão ficar os resultados
	mkdir -p ../tests/$teste
		
	#Executa as instâncias para todas as seeds
	for instance in ${instances[@]} ; do
			#echo "Instancia $instance"			
			instancia=../instances/$instance.ctt
			
			for seed in ${seeds[@]} ; do
				#echo "Seed $seed"
				resultado=../results/$teste.$instance.$seed.res
				saida=../tests/$teste/$instance-$seed.dat
				
				#Simple Neighborhood tests
				if [ $teste = TT_M ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=1 const=1 grafico=1 > $saida
				elif [ $teste = TT_S ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=2 const=1 grafico=1 > $saida
				elif [ $teste = TT_KS ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=3 const=1 grafico=1 > $saida
				elif [ $teste = TT_KE ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=15 const=1 grafico=1 > $saida
				elif [ $teste = TT_TM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=4 const=1 grafico=1 > $saida
				elif [ $teste = TT_RM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=5 const=1 grafico=1 > $saida
				elif [ $teste = TT_LM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=10 const=1 grafico=1 > $saida
				elif [ $teste = TT_RS ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=7 const=1 grafico=1 > $saida
				elif [ $teste = TT_MWD ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=8 const=1 grafico=1 > $saida

				#Combined Neighborhood Tests
				elif [ $teste = TT_MS ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=0 const=1 grafico=1 > $saida
				elif [ $teste = TT_MS_KE ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=50 const=1 grafico=1 > $saida
				elif [ $teste = TT_MS_MWD ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=51 const=1 grafico=1 > $saida
				elif [ $teste = TT_MS_RSM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=52 const=1 grafico=1 > $saida
				elif [ $teste = TT_LM_KE ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=53 const=1 grafico=1 > $saida
				elif [ $teste = TT_LM_MWD ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=54 const=1 grafico=1 > $saida
				elif [ $teste = TT_LM_RSM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=55 const=1 grafico=1 > $saida
				elif [ $teste = TT_LM_RM_TM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=59 const=1 grafico=1 > $saida
				elif [ $teste = TT_LM_RM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=60 const=1 grafico=1 > $saida
				elif [ $teste = TT_LM_TM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=61 const=1 grafico=1 > $saida
				elif [ $teste = TT_RM_TM ]; then
					./grasp $instancia $resultado maxIter=1 seed=$seed bl=sd viz=62 const=1 grafico=1 > $saida
					
				else
				  echo "Invalid test ($teste)"
				fi

			done
			cat ../tests/$teste/$instance-* > ../tests/summaryperinstance/$teste-$instance.dat
	done
done

echo "done"

