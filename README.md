# Solver for Course-based University Timetabling Problem

This is a multi-start solver for the Course-based University Timetabling Problem.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* g++ compiler
* These instructions are good for running the solver in a UNIX system and it was only tested in Ubuntu.

## Running the tests

Watch the file named "testscript.sh". It is the test script used to obtain the results in the paper submitted. *Note that for this paper we didn't use the multi-start feature of this code, instead we runned one iteration of the solver many times*

To compile and run the tests simply execute the script
> ./testscript.sh

You may need to change the access permission to allow it to execute first:
> chmod +x testscript.sh

This test script were made to gather the information about the neighborhoods implemented and store in files.

The information gathered in the tests are the following:

* Seed
* Cost of the constructed solution
* Time spent on construction phase
* Cost of final solution
* Time spent on local search phase
* Number of iterations of the local search method
* Number of movements accepted during the local search
* Number of created movements that are valid *(new solution after movement does not satisfy all hard constraints)*
* Number of created movements that improves a solution 
* Number of created movements that are invalid *(new solution after movement does not satisfy a hard constraint)*
* Total number of created movements
* Total time spent on the solver iteration
* Number of the current solver iteration


The information gathered are saved in files in the directory *tests/summaryperinstance/*. Each iteration of the solver is saved as a line in a output test file containing the above information separeted by a semicolon character. Each file contains information about many iterations of one neighborhood and one instance indicated by the file name.

## Running an instance separatedly

Go to src directory
> cd src

Compile the code 
> make

run
> ./grasp infilename outfilename [params]


* infilename : name of instance file. *Usually instance/comp\**
* outfilename: name of output file.
* params are optional and they can be:
	* Seed value - set to TIME(NULL) if not informed
		* **seed=SEED_VALUE**
	* Number of solver iterations (for multi-start feature)
		* **maxIter=NUMBER**
	* Limit timeout in seconds
		* **timeout=TIME**
	* Initial solution construction
		* **const=0** - GRASP construction (threshold = 0.10) 
		* **const=1** - greedy construction
		* **const=2** - greedy construction with some ramdomness
	* Local search algorithm
		* **bl=sd**   - Steepest Descent (SD)
		* **bl=hc**   - Hill Climbing (HC)
		* **bl=sa**   - Simulated Annealing (SA)
	* HC parameters:
		* **maxhc=NUMBER**  - maximum number of HC iterations
		* **n=NUMBER**      - maximum number of HC iterations without improvement
		* **k=NUMBER**      - number of random neighbors generated each HC iteration
	* SA parameters:
		* **t0=NUMBER**     - Initial temperature
		* **tFinal=NUMBER** - Final temperature
		* **beta=NUMBER**   - Cooling rate			
	* Debug print
		* **info=1**
	* Statistics print
		* **grafico=1**
	* Neighborhood choice
		* **viz=0**  - Move U Swap (50% move, 50% swap in HC and SA)
		* **viz=1**  - Move
		* **viz=2**  - Swap
		* **viz=3**  - Simple Kempe Chain (KS)
		* **viz=15** - Extended Kempe Chain (KE)
		* **viz=4**  - Time Move (TM)
		* **viz=5**  - Room Move (RM)
		* **viz=10** - Lecture Move (LM)
		* **viz=7**  - Room Stability Move (RSM)
		* **viz=8**  - Minimum Working Days Move (MWD)
		* **viz=53** - LM -> KE
		* **viz=54** - LM -> MWD
		* **viz=55** - LM -> RSM
		* **viz=59** - LM U RM U TM
		* **viz=60** - LM U RM
		* **viz=61** - LM U TM
		* **viz=62** - RM U TM

Example of a run:
> ./grasp instances/comp05.ctt output.res timeout=200 bl=sa viz=0 const=0 info=1 t0=20.867 tFinal=0.005 beta=0.9999


## Versioning

This is the version used for the work submitted to the ICCSA 2017 - 17th International Conference on Computational Science and Its Applications 

## Authors

* **Edmar Kampke** -  *Adding other neighborhoods* 
* **Erika Segatto** - *Restructuring of the code and adding Kempe chain neighborhood* 
* **Wallace Rocha** - *Initial work* - [github](https://github.com/walacesrocha/Timetabling)


## Acknowledgments

* Federal University of Espírito Santo - UFES
* Labotim - Laboratório de Otimização
* Fundação de Amparo à Pesquisa e Inovação do Espírito Santo - FAPES
* Conselho Nacional de Desenvolvimento Científico e Tecnológico - CNPq
* ITC 2007 international timetabling competition