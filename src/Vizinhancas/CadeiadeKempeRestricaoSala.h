/*
 * CadeiadeKempeRestricaoSala.h
 *
 *  Created on: 21/06/2016
 *      Author: erika
 */

#ifndef DISS_VETOR_VIZINHANCAS_CADEIADEKEMPERESTRICAOSALA_H_
#define DISS_VETOR_VIZINHANCAS_CADEIADEKEMPERESTRICAOSALA_H_

#include "Movimento.h"

#include <list>
using namespace std;

class Individuo;
class Problema;
class Alocacao;
class opAloc;
class CadeiadeKempeRestricaoSala: public Movimento {
public:
	list< opAloc* > t1; 			//Lista com todos os timeslots disponíveis para a troca do horário t1
	list< opAloc* > t2; 			//Lista com todos os timeslots disponíveis para a troca do horário t2

	list< opAloc* > t1Ind; 			//Lista com todos os timeslots indisponíveis para a troca do horário t1
	list< opAloc* > t2Ind; 			//Lista com todos os timeslots indisponíveis para a troca do horário t2

	list< opAloc* > trocast1; 		//Lista com todos os timeslots escolhidos para a troca do horário t1
	list< opAloc* > trocast2; 		//Lista com todos os timeslots escolhidos para a troca do horário t2

	list< Movimento* > movimentos; 	//Lista com todos os movimentos montada a partir de trocast1 e trocast2

	CadeiadeKempeRestricaoSala(Problema* p, Individuo* piInd);
	CadeiadeKempeRestricaoSala(Problema* p, Individuo* piInd, int piHorario1, int piHorario2);
	virtual ~CadeiadeKempeRestricaoSala();
	void aplicaMovimento();

private:
	int calculaDeltaFit(Problema* p);
	void aplicaMoveSemRecalculoFuncaoObjetivo();
	void desaplicaMoveSemRecalculoFuncaoObjetivo();
	int KMP_preencheListaMovimentosTroca();
	int KMP_temRestricao(Alocacao* p1, Alocacao* p2);
	int ObtemMelhorPosicaoA(int** matrizConflitos);

	int ObtemMelhorPosicaoB(int** Conflitos, int tamConflitos);
	int TraduzVetorParaPosicoesDaMatrizNaTabelaHorario(int PosicoesComConflitos[][2], int nPosComConflitoA, int nPosComConflitoB);
	int PreencheVetorCadeiaKempe(int** Conflitos, int tamConflitos, int posInicial);
	void AtualizaListaDisponiveis();

	void imprimeVetorTrocas(list< opAloc* > vet);
};

#endif /* DISS_VETOR_VIZINHANCAS_CADEIADEKEMPERESTRICAOSALA_H_ */
