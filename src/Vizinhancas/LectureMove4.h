/*
 * LectureMove4.h
 *
 *  Created on: Mar 16, 2016
 *      Author: renan
 */


#ifndef LectureMove4_H_
#define LectureMove4_H_

#include "Movimento.h"
#include "Swap.h"
#include "Move.h"
#include "TimeMove.h"
#include "../Model/Individuo.h"
#include "../Model/Problema.h"

class LectureMove4 : public Movimento{
public:
	LectureMove4(Problema* p, Individuo* piInd);
	LectureMove4(Problema *pro, Individuo* piInd, int posAulaAlocada, int posTodosHorarios);
	~LectureMove4();
	void aplicaMovimento();

private:
	Problema* p;
	Movimento* m;
};

#endif /* LectureMove4_H_ */
