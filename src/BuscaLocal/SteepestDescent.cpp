/*
 * HillClimbing.cpp
 *
 *  Created on: May 26, 2015
 *      Author: erika
 */

#include <stdlib.h>
#include <vector>

#include "SteepestDescent.h"
#include "../Vizinhancas/Move.h"
#include "../Vizinhancas/Swap.h"
#include "../Vizinhancas/TimeMove.h"
#include "../Vizinhancas/TimeMove2.h"
#include "../Vizinhancas/CadeiadeKempe.h"
#include "../Vizinhancas/CadeiadeKempeRestricaoSala.h"
#include "../Vizinhancas/CadeiaKempeCompletaSalaVazia.h"
#include "../Vizinhancas/CadeiadeKempeEstendidaRestrSala.h"
#include "../Vizinhancas/CadeiadeKempeExtendido.h"
#include "../Vizinhancas/RoomMove.h"
#include "../Vizinhancas/RoomMove2.h"
#include "../Vizinhancas/LectureMove.h"
#include "../Vizinhancas/LectureMove2.h"
#include "../Vizinhancas/LectureMove3.h"
#include "../Vizinhancas/LectureMove4.h"
#include "../Vizinhancas/RoomStabilityMove.h"
#include "../Vizinhancas/MinWorkingDaysMove.h"
#include "../Vizinhancas/CurriculumCompactnessMove.h"

const int _MOVE = 1;
const int _SWAP = 2;
const int _KEMPE = 3;
const int _TIMEMOVE = 4;
const int _ROOMMOVE = 5;
const int _LECTUREMOVE = 6;
const int _ROOMSTABILITYMOVE = 7;
const int _MINWORKINGDAYSMOVE = 8;
const int _CURRICULUMCOMPACTNESSMOVE = 9;
const int _LECTURE3 = 10;
const int _LECTURE4 = 11;
const int _KEMPEEXT = 12;
const int _KEMPERESTSALA  = 13;
const int _KEMPESALAVAZIA = 14;
const int _KEMPEEXTRESTSALA = 15;
const int _ROOMMOVE2 = 16;
const int _LECTURE2 = 17;

const int _MOVE_SWAP = 0;
const int _MOVE_SWAP_KEMPE_30 = 20;
const int _MOVE_SWAP_KEMPE_20 = 21;
const int _MOVE_SWAP_KEMPE_10 = 22;
const int _TIME_ROOM = 30;

#define N 50

SteepestDescent::SteepestDescent(Problema* p) {
	problema = p;
}

SteepestDescent::~SteepestDescent() { }



Individuo* SteepestDescent::executaSDKempeEstendidoRestricaoSala(Individuo* ind, int nIterMaxSD){
	Movimento* mov;
	int h1, h2;
	vector<Movimento*> melhorMov;
//	int continua = 1;
	int opEscolhida;

	//Cria movimento
//	fprintf(stderr, "executaSDKempeEstendidoRestricaoSala(%d, %d)\n", ind->fitness, nIterMaxSD);
	while (nIterMaxSD--) {
		nIteracoesExecutadas++;

		for (h1 = 0; h1 != ind->p->nHorarios; h1++) {
			for (h2 = 0; h2 != ind->p->nHorarios; h2++) {
				if (h1 != h2) {
					mov = new CadeiadeKempeEstendidaRestrSala(ind->p, ind, h1, h2);

					nKempes++;
					if (mov->deltaFit < 0)         nKempesMelhora++;
					else if (mov->deltaFit < 2000) nKempesValidos++;
					else                           nKempesInvalido++;
					if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
						while ( !melhorMov.empty()){
							delete(melhorMov.back());
							melhorMov.pop_back();
						}
						melhorMov.push_back(mov);
					} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
						melhorMov.push_back(mov);
					} else
						delete (mov);
				}
			}
		}

//		if (melhorMov[0]->deltaFit < 0) {
//			fprintf(stderr, "%d\n", (melhorMov[0]->deltaFit * (-1)));
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
//		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

//		if (!continua) break;
	};
	
	return ind;
}

Individuo* SteepestDescent::executaSDMinWorkingDays(Individuo* ind, int nIterMax){
	Movimento* mov;
	vector<Alocacao*>::iterator itLect1;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;

	//Cria movimento
	while (nIterMax--) {
		nIteracoesExecutadas++;

		for (itLect1 = ind->aulasAlocadas.begin(); itLect1 != ind->aulasAlocadas.end(); itLect1++) {

			mov = new MinWorkingDaysMove(problema, ind, *itLect1);
			nMinWorkingDaysMoves++;
			if (mov->deltaFit < 0) 		   nMinWorkingDaysMovesMelhora++;
			else if (mov->deltaFit < 2000) nMinWorkingDaysMovesValidos++;
			else 						   nMinWorkingDaysMovesInvalido++;
			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;

	};
	
	return ind;
}

Individuo* SteepestDescent::executaSDRoomStabilityMove(Individuo* ind, int nIterMax){
	Movimento* mov;
	int itLect;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;

	//Cria movimento
	while (nIterMax--) {
		nIteracoesExecutadas++;

		for (itLect = 0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {

			mov = new RoomStabilityMove(problema, ind, itLect);
			nRoomStabilityMoves++;
			if (mov->deltaFit < 0) 		   nRoomStabilityMovesMelhora++;
			else if (mov->deltaFit < 2000) nRoomStabilityMovesValidos++;
			else 						   nRoomStabilityMovesInvalido++;
			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;

	};
	
	return ind;
}

Individuo* SteepestDescent::executaSDMoveSwap(Individuo* ind, int nIterMax){

	Movimento* mov;
	int itEmpty, itLect1, itLect2;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {
		nIteracoesExecutadas++;

		for (itLect1=0; itLect1 < (int)ind->aulasAlocadas.size(); itLect1++) {
			for (itEmpty=0; itEmpty < (int)ind->horariosVazios.size(); itEmpty++) {
				mov = new Move(ind, itLect1, itEmpty);
				nMoves++;
				if (mov->deltaFit < 0) 			nMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nMovesValidos++;
				else 							nMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
			for (itLect2=0; itLect2 < (int)ind->aulasAlocadas.size(); itLect2++) {
				mov = new Swap(ind, itLect2, itLect1);
				nSwaps++;
				if (mov->deltaFit < 0) 			nSwapsMelhora++;
				else if (mov->deltaFit < 2000) 	nSwapsValidos++;
				else 							nSwapsInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}


Individuo* SteepestDescent::executaSDMoveSwap(Individuo* ind){

	Movimento* mov;
	int itEmpty, itLect1, itLect2;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect1=0; itLect1 < (int)ind->aulasAlocadas.size(); itLect1++) {
			for (itEmpty=0; itEmpty < (int)ind->horariosVazios.size(); itEmpty++) {
				mov = new Move(ind, itLect1, itEmpty);
				nMoves++;
				if (mov->deltaFit < 0) 			nMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nMovesValidos++;
				else 							nMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
			for (itLect2=0; itLect2 < (int)ind->aulasAlocadas.size(); itLect2++) {
				mov = new Swap(ind, itLect2, itLect1);
				nSwaps++;
				if (mov->deltaFit < 0) 			nSwapsMelhora++;
				else if (mov->deltaFit < 2000) 	nSwapsValidos++;
				else 							nSwapsInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDMove(Individuo* ind) {
	Move* mov;
	int itEmpty, itLect;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itEmpty=0; itEmpty < (int)ind->horariosVazios.size(); itEmpty++) {
			for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
				mov = new Move(ind, itLect, itEmpty);
				nMoves++;
				if (mov->deltaFit < 0) 			nMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nMovesValidos++;
				else 							nMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDSwap(Individuo* ind) {
	Movimento* mov;
	int itLect1, itLect2;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect1=0; itLect1<(int)ind->aulasAlocadas.size(); itLect1++) {
			for (itLect2=0; itLect2<(int)ind->aulasAlocadas.size(); itLect2++) {
				mov = new Swap(ind, itLect2, itLect1);
				nSwaps++;
				if (mov->deltaFit < 0) 			nSwapsMelhora++;
				else if (mov->deltaFit < 2000) 	nSwapsValidos++;
				else 							nSwapsInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}
		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;

	};
	
	return ind;
}

Individuo* SteepestDescent::executaSDKempe(Individuo* ind) {
	Movimento* mov;
	int h1, h2;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (h1 = 0; h1 < (int)ind->aulasAlocadas.size(); h1++) {
			for (h2 = 0; h2 < (int)ind->aulasAlocadas.size(); h2++) {
				if (ind->aulasAlocadas[h1]->aula->disciplina->numeroSequencial != ind->aulasAlocadas[h2]->aula->disciplina->numeroSequencial) {
					mov = new CadeiadeKempe(ind->p, ind, h1, h2);

					nKempes++;
					if (mov->deltaFit < 0)         nKempesMelhora++;
					else if (mov->deltaFit < 2000) nKempesValidos++;
					else                           nKempesInvalido++;

					if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
						while ( !melhorMov.empty()){
							delete(melhorMov.back());
							melhorMov.pop_back();
						}
						melhorMov.push_back(mov);
					} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
						melhorMov.push_back(mov);
					} else
						delete (mov);
				}
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};
	
	return ind;
}

Individuo* SteepestDescent::executaSDKempeRestricaoSala(Individuo* ind) {
	Movimento* mov;
	int h1, h2;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (h1 = 0; h1 != ind->p->nHorarios; h1++) {
			for (h2 = 0; h2 != ind->p->nHorarios; h2++) {
				if (h1 != h2) {
					mov = new CadeiadeKempeRestricaoSala(ind->p, ind, h1, h2);

					nKempes++;
					if (mov->deltaFit < 0)         nKempesMelhora++;
					else if (mov->deltaFit < 2000) nKempesValidos++;
					else                           nKempesInvalido++;

					if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
						while ( !melhorMov.empty()){
							delete(melhorMov.back());
							melhorMov.pop_back();
						}
						melhorMov.push_back(mov);
					} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
						melhorMov.push_back(mov);
					} else
						delete (mov);
				}
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};
	
	return ind;
}

Individuo* SteepestDescent::executaSDKempeSalaVazia(Individuo* ind) {
	Movimento* mov;
	int h1, h2;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (h1 = 0; h1 != ind->p->nHorarios; h1++) {
			for (h2 = 0; h2 != ind->p->nHorarios; h2++) {
				if (h1 != h2) {
					mov = new CadeiaKempeCompletaSalaVazia(ind->p, ind, h1, h2);

					nKempes++;
					if (mov->deltaFit < 0)         nKempesMelhora++;
					else if (mov->deltaFit < 2000) nKempesValidos++;
					else                           nKempesInvalido++;

					if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
						while ( !melhorMov.empty()){
							delete(melhorMov.back());
							melhorMov.pop_back();
						}
						melhorMov.push_back(mov);
					} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
						melhorMov.push_back(mov);
					} else
						delete (mov);
				}
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};
	
	return ind;
}

Individuo* SteepestDescent::executaSDKempeEstendido(Individuo* ind) {
	Movimento* mov;
	int h1, h2;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (h1 = 0; h1 != ind->p->nHorarios; h1++) {
			for (h2 = 0; h2 != ind->p->nHorarios; h2++) {

				if (h1 != h2) {
					mov = new CadeiadeKempeExtendido(ind->p, ind, h1, h2);
					nKempes++;
					if (mov->deltaFit < 0)         nKempesMelhora++;
					else if (mov->deltaFit < 2000) nKempesValidos++;
					else                           nKempesInvalido++;

					if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
						while ( !melhorMov.empty()){
							delete(melhorMov.back());
							melhorMov.pop_back();
						}
						melhorMov.push_back(mov);
					} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
						melhorMov.push_back(mov);
					} else
						delete (mov);
				}
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;

	};
	return ind;
}

Individuo* SteepestDescent::executaSDKempeEstendidoRestricaoSala(Individuo* ind) {
	Movimento* mov;
	int h1, h2;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (h1 = 0; h1 != ind->p->nHorarios; h1++) {
			for (h2 = 0; h2 != ind->p->nHorarios; h2++) {
				if (h1 != h2) {
					mov = new CadeiadeKempeEstendidaRestrSala(ind->p, ind, h1, h2);

					nKempes++;
					if (mov->deltaFit < 0)         nKempesMelhora++;
					else if (mov->deltaFit < 2000) nKempesValidos++;
					else                           nKempesInvalido++;
					if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
						while ( !melhorMov.empty()){
							delete(melhorMov.back());
							melhorMov.pop_back();
						}
						melhorMov.push_back(mov);
					} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
						melhorMov.push_back(mov);
					} else
						delete (mov);
				}
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};
	
	return ind;
}


Individuo* SteepestDescent::executaSDTimeMove(Individuo* ind) {
	TimeMove* mov;
	int itLect;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			mov = new TimeMove(ind, itLect);
			nTimeMoves++;
			if (mov->deltaFit < 0) 			nTimeMovesMelhora++;
			else if (mov->deltaFit < 5000) 	nTimeMovesValidos++;
			else 							nTimeMovesInvalido++;

			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);			
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDTimeMove(Individuo* ind, int nIterMax) {
	TimeMove* mov;
	int itLect;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			mov = new TimeMove(ind, itLect);
			nTimeMoves++;
			if (mov->deltaFit < 0) 			nTimeMovesMelhora++;
			else if (mov->deltaFit < 5000) 	nTimeMovesValidos++;
			else 							nTimeMovesInvalido++;

			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);			
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDLectureMove1(Individuo* ind) {
	LectureMove* mov;
	int itLect, itTodosHorarios;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;
	vector<int> horariosPossiveis;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			horariosPossiveis.clear();
			horariosPossiveis.reserve( (int)ind->TodosHorarios.size() );
			for(itTodosHorarios=0; itTodosHorarios < (int)ind->TodosHorarios.size(); itTodosHorarios++){
				if(
					!ind->p->HorarioIndisponivelDisciplina(ind->aulasAlocadas[itLect]->aula->disciplina, ind->TodosHorarios[itTodosHorarios]->horario->horario) &&
					(
						ind->TodosHorarios[itTodosHorarios]->aula == NULL || 
						(ind->aulasAlocadas[itLect]->aula->disciplina->numeroSequencial != ind->TodosHorarios[itTodosHorarios]->aula->disciplina->numeroSequencial)
					)
				  ){
						horariosPossiveis.push_back(itTodosHorarios);
				}
			}


			for (itTodosHorarios=0; itTodosHorarios < (int)horariosPossiveis.size(); itTodosHorarios++) {
				mov = new LectureMove(ind->p, ind, itLect, itTodosHorarios);
				nLectureMoves++;
				if (mov->deltaFit < 0) 			nLectureMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nLectureMovesValidos++;
				else 							nLectureMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDLectureMove2(Individuo* ind) {
	LectureMove2* mov;
	int itLect, itTodosHorarios;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;
	vector<int> horariosPossiveis;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			horariosPossiveis.clear();
			horariosPossiveis.reserve( (int)ind->TodosHorarios.size() );
			for(itTodosHorarios=0; itTodosHorarios < (int)ind->TodosHorarios.size(); itTodosHorarios++){
				if(
					!ind->p->HorarioIndisponivelDisciplina(ind->aulasAlocadas[itLect]->aula->disciplina, ind->TodosHorarios[itTodosHorarios]->horario->horario) &&
					(
						ind->TodosHorarios[itTodosHorarios]->aula == NULL || 
						(ind->aulasAlocadas[itLect]->aula->disciplina->numeroSequencial != ind->TodosHorarios[itTodosHorarios]->aula->disciplina->numeroSequencial)
					)
				  ){
						horariosPossiveis.push_back(itTodosHorarios);
				}
			}


			for (itTodosHorarios=0; itTodosHorarios < (int)horariosPossiveis.size(); itTodosHorarios++) {
				mov = new LectureMove2(ind->p, ind, itLect, itTodosHorarios);
				nLectureMoves++;
				if (mov->deltaFit < 0) 			nLectureMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nLectureMovesValidos++;
				else 							nLectureMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDLectureMove3(Individuo* ind) {
	LectureMove3* mov;
	int itLect, itTodosHorarios;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			for (itTodosHorarios=0; itTodosHorarios < (int)ind->TodosHorarios.size(); itTodosHorarios++) {
				mov = new LectureMove3(ind->p, ind, itLect, itTodosHorarios);
				nLectureMoves++;
				if (mov->deltaFit < 0) 			nLectureMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nLectureMovesValidos++;
				else 							nLectureMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDLectureMove3(Individuo* ind, int nIterMax) {
	LectureMove3* mov;
	int itLect, itTodosHorarios;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			for (itTodosHorarios=0; itTodosHorarios < (int)ind->TodosHorarios.size(); itTodosHorarios++) {
				mov = new LectureMove3(ind->p, ind, itLect, itTodosHorarios);
				nLectureMoves++;
				if (mov->deltaFit < 0) 			nLectureMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nLectureMovesValidos++;
				else 							nLectureMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDLectureMove4(Individuo* ind) {
	LectureMove4* mov;
	int itLect, itTodosHorarios;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			for (itTodosHorarios=0; itTodosHorarios < (int)ind->TodosHorarios.size(); itTodosHorarios++) {
				mov = new LectureMove4(ind->p, ind, itLect, itTodosHorarios);
				nLectureMoves++;
				if (mov->deltaFit < 0) 			nLectureMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nLectureMovesValidos++;
				else 							nLectureMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDLectureMove3e4(Individuo* ind) {
	Movimento* mov;
	int itLect, itTodosHorarios;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	float opLM;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			for (itTodosHorarios=0; itTodosHorarios < (int)ind->TodosHorarios.size(); itTodosHorarios++) {
				opLM = rand() / RAND_MAX;
				if (opLM <= 0.5 )
					mov = new LectureMove3(ind->p, ind, itLect, itTodosHorarios);
				else
					mov = new LectureMove4(ind->p, ind, itLect, itTodosHorarios);
				nLectureMoves++;
				if (mov->deltaFit < 0) 			nLectureMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nLectureMovesValidos++;
				else 							nLectureMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDLectureMove3e4(Individuo* ind, int nIterMax) {
	Movimento* mov;
	int itLect, itTodosHorarios;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	float opLM;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			for (itTodosHorarios=0; itTodosHorarios < (int)ind->TodosHorarios.size(); itTodosHorarios++) {
				opLM = rand() / RAND_MAX;
				if (opLM <= 0.5 )
					mov = new LectureMove3(ind->p, ind, itLect, itTodosHorarios);
				else
					mov = new LectureMove4(ind->p, ind, itLect, itTodosHorarios);
				nLectureMoves++;
				if (mov->deltaFit < 0) 			nLectureMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nLectureMovesValidos++;
				else 							nLectureMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}


Individuo* SteepestDescent::executaSDRoomMove(Individuo* ind) {
	RoomMove* mov;
	int itLect, itEmpty;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			for (itEmpty=0; itEmpty < (int)ind->horariosVazios.size(); itEmpty++) {
				mov = new RoomMove(ind, itLect, itEmpty);
				nRoomMoves++;
				if (mov->deltaFit < 0) 			nRoomMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nRoomMovesValidos++;
				else 							nRoomMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDRoomMove(Individuo* ind, int nIterMax) {
	RoomMove* mov;
	int itLect;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			mov = new RoomMove(ind, itLect);
			nRoomMoves++;
			if (mov->deltaFit < 0) 			nRoomMovesMelhora++;
			else if (mov->deltaFit < 5000) 	nRoomMovesValidos++;
			else 							nRoomMovesInvalido++;

			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDRoomMove2(Individuo* ind) {
	RoomMove2* mov;
	int itLect;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			mov = new RoomMove2(ind->p, ind, itLect);
			nRoomMoves++;
			if (mov->deltaFit < 0) 			nRoomMovesMelhora++;
			else if (mov->deltaFit < 5000) 	nRoomMovesValidos++;
			else 							nRoomMovesInvalido++;

			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDMinWorkingDays(Individuo* ind) {
	Movimento* mov;
	vector<Alocacao*>::iterator itLect1;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect1 = ind->aulasAlocadas.begin(); itLect1 != ind->aulasAlocadas.end(); itLect1++) {

			mov = new MinWorkingDaysMove(problema, ind, *itLect1);
			nMinWorkingDaysMoves++;
			if (mov->deltaFit < 0) 		   nMinWorkingDaysMovesMelhora++;
			else if (mov->deltaFit < 2000) nMinWorkingDaysMovesValidos++;
			else 						   nMinWorkingDaysMovesInvalido++;
			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;

	};
	
	return ind;
}

Individuo* SteepestDescent::executaSDRoomStabilityMove(Individuo* ind) {
	Movimento* mov;
	int itLect;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;

	//Cria movimento
	while (1) {
		nIteracoesExecutadas++;

		for (itLect = 0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {

			mov = new RoomStabilityMove(problema, ind, itLect);
			nRoomStabilityMoves++;
			if (mov->deltaFit < 0) 		   nRoomStabilityMovesMelhora++;
			else if (mov->deltaFit < 2000) nRoomStabilityMovesValidos++;
			else 						   nRoomStabilityMovesInvalido++;
			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);
		}

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;

	};
	
	return ind;
}


Individuo* SteepestDescent::executaSDLMRM(Individuo* ind, int nIterMax){
	Movimento* mov;
	int itLect, itTodosHorarios;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			for (itTodosHorarios=0; itTodosHorarios < (int)ind->TodosHorarios.size(); itTodosHorarios++) {
				mov = new LectureMove3(ind->p, ind, itLect, itTodosHorarios);
				nLectureMoves++;
				if (mov->deltaFit < 0) 			nLectureMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nLectureMovesValidos++;
				else 							nLectureMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}
		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			mov = new RoomMove(ind, itLect);
			nRoomMoves++;
			if (mov->deltaFit < 0) 			nRoomMovesMelhora++;
			else if (mov->deltaFit < 5000) 	nRoomMovesValidos++;
			else 							nRoomMovesInvalido++;

			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);
		}
		

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDLMTM(Individuo* ind, int nIterMax){
	Movimento* mov;
	int itLect, itTodosHorarios;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			for (itTodosHorarios=0; itTodosHorarios < (int)ind->TodosHorarios.size(); itTodosHorarios++) {
				mov = new LectureMove3(ind->p, ind, itLect, itTodosHorarios);
				nLectureMoves++;
				if (mov->deltaFit < 0) 			nLectureMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nLectureMovesValidos++;
				else 							nLectureMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		}
		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			mov = new TimeMove(ind, itLect);
			nTimeMoves++;
			if (mov->deltaFit < 0) 			nTimeMovesMelhora++;
			else if (mov->deltaFit < 5000) 	nTimeMovesValidos++;
			else 							nTimeMovesInvalido++;

			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);			
		}
		

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDTMRM(Individuo* ind, int nIterMax){
	Movimento* mov;
	int itLect;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			// RM
			mov = new RoomMove(ind, itLect);
			nRoomMoves++;
			if (mov->deltaFit < 0) 			nRoomMovesMelhora++;
			else if (mov->deltaFit < 5000) 	nRoomMovesValidos++;
			else 							nRoomMovesInvalido++;

			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);
			// TM
			mov = new TimeMove(ind, itLect);
			nTimeMoves++;
			if (mov->deltaFit < 0) 			nTimeMovesMelhora++;
			else if (mov->deltaFit < 5000) 	nTimeMovesValidos++;
			else 							nTimeMovesInvalido++;

			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);	
		}
		

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}

Individuo* SteepestDescent::executaSDLMRMTM(Individuo* ind, int nIterMax){
	Movimento* mov;
	int itLect, itTodosHorarios;
	vector<Movimento*> melhorMov;
	int continua = 1;
	int opEscolhida;
	nIteracoesExecutadas = 0;

	//Cria movimento
	while (nIterMax--) {
		nIteracoesExecutadas++;

		for (itLect=0; itLect < (int)ind->aulasAlocadas.size(); itLect++) {
			
			
			mov = new RoomMove(ind, itLect);
			nSwaps++;
			if (mov->deltaFit < 0) 			nSwapsMelhora++;
			else if (mov->deltaFit < 2000) 	nSwapsValidos++;
			else 							nSwapsInvalido++;
			
			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);
				
			mov = new TimeMove(ind, itLect);
			nKempes++;
			if (mov->deltaFit < 0)         nKempesMelhora++;
			else if (mov->deltaFit < 2000) nKempesValidos++;
			else                           nKempesInvalido++;

			if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
				while ( !melhorMov.empty()){
					delete(melhorMov.back());
					melhorMov.pop_back();
				}
				melhorMov.push_back(mov);
			} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
				melhorMov.push_back(mov);
			} else
				delete (mov);	
				
			for (itTodosHorarios=0; itTodosHorarios < (int)ind->TodosHorarios.size(); itTodosHorarios++) {
				mov = new LectureMove3(ind->p, ind, itLect, itTodosHorarios);
				nLectureMoves++;
				if (mov->deltaFit < 0) 			nLectureMovesMelhora++;
				else if (mov->deltaFit < 5000) 	nLectureMovesValidos++;
				else 							nLectureMovesInvalido++;

				if ( melhorMov.empty() || (melhorMov[0]->deltaFit > mov->deltaFit) ) {
					while ( !melhorMov.empty()){
						delete(melhorMov.back());
						melhorMov.pop_back();
					}
					melhorMov.push_back(mov);
				} else if( melhorMov[0]->deltaFit == mov->deltaFit ){
					melhorMov.push_back(mov);
				} else
					delete (mov);
			}
		
		}
		

		if (melhorMov[0]->deltaFit < 0) {
			opEscolhida = rand() % (int)melhorMov.size();
			melhorMov[opEscolhida]->aplicaMovimento();
			nMovimentosRealizados++;
		} else continua = 0;
		while ( !melhorMov.empty()){
			delete(melhorMov.back());
			melhorMov.pop_back();
		}

		if (!continua) break;
	};

	
	return ind;
}


Individuo* SteepestDescent::executa(Individuo* bestInd, const int movimento) {
	switch (movimento) {
	case _MOVE_SWAP:
		bestInd = executaSDMoveSwap(bestInd);
		break;
	case _MOVE:
		bestInd = executaSDMove(bestInd);
		break;
	case _SWAP:
		bestInd = executaSDSwap(bestInd);
		break;
	case _KEMPE:
		bestInd = executaSDKempe(bestInd);
		break;
	case _KEMPEEXT:
		bestInd = executaSDKempeEstendido(bestInd);
		break;
	case _KEMPERESTSALA:
		bestInd = executaSDKempeRestricaoSala(bestInd);
		break;
	case _KEMPESALAVAZIA:
		bestInd = executaSDKempeSalaVazia(bestInd);
		break;
	case _KEMPEEXTRESTSALA:
		bestInd = executaSDKempeEstendidoRestricaoSala(bestInd);
		break;
	case _TIMEMOVE:
		bestInd = executaSDTimeMove(bestInd);
		break;
	case _ROOMMOVE:
		bestInd = executaSDRoomMove(bestInd);
		break;
	case _ROOMMOVE2:
		bestInd = executaSDRoomMove2(bestInd);
		break;
	case _LECTUREMOVE:
		bestInd = executaSDLectureMove1(bestInd);
		break;
	case _LECTURE2:
		bestInd = executaSDLectureMove2(bestInd);
		break;
	case _LECTURE3:
		bestInd = executaSDLectureMove3(bestInd);
		break;
	case _LECTURE4:
		bestInd = executaSDLectureMove4(bestInd);
		break;
	case _ROOMSTABILITYMOVE:
		bestInd = executaSDRoomStabilityMove(bestInd);
		break;
	case _MINWORKINGDAYSMOVE:
		bestInd = executaSDMinWorkingDays(bestInd);
		break;
//		case _CURRICULUMCOMPACTNESSMOVE:
//					break;
	default:
		fprintf(stderr, "Movimento %d invalido!\n\n", movimento);
		fprintf(stderr, "Movimentos validos para o SD:\n");
		fprintf(stderr, "\tMOVE  (%d)\n\tSWAP  (%d)\n", _MOVE, _SWAP);
		fprintf(stderr, "\tKempe Simples(%d)\n\tKempe estendido (%d)\n", _KEMPE, _KEMPEEXT);
		fprintf(stderr, "\tKempe adiciona sala vazia (%d)\n\tKempe com restricao de sala (%d)\n", _KEMPESALAVAZIA, _KEMPERESTSALA);
		fprintf(stderr, "\tKempe estendido com restrição de sala (%d)\n", _KEMPEEXTRESTSALA);
		fprintf(stderr, "\tTimeMove   (%d)\n", _TIMEMOVE);
		fprintf(stderr, "\tRoomMove 1-2 (%d, %d)\n", _ROOMMOVE, _ROOMMOVE2);
		fprintf(stderr, "\tLectureMove 1-4 (%d, %d, %d, %d)\n", _LECTUREMOVE, _LECTURE2, _LECTURE3, _LECTURE4);
		fprintf(stderr, "\tRoomStabilityMove  (%d)\n\t \n", _ROOMSTABILITYMOVE);
		fprintf(stderr, "\tMinWorkingDaysMove (%d)\n\t \n", _MINWORKINGDAYSMOVE);
		exit(0);
	}

	return bestInd;
}

void SteepestDescent::imprimeExecucao() {
	printf("\nNumero de Movimentos Realizados: %d\n", nMovimentosRealizados);
	printf(
			"Total de Movimentos   :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n",
			nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves,
			nRoomStabilityMoves, nMinWorkingDaysMoves,
			nCurriculumCompactnessMoves);
	printf(
			"Movimentos Invalidos  :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n",
			nMovesInvalido, nSwapsInvalido, nKempesInvalido, nTimeMovesInvalido,
			nRoomMovesInvalido, nLectureMovesInvalido,
			nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido,
			nCurriculumCompactnessMovesInvalido);
	printf(
			"Movimentos Validos    :  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n",
			nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos,
			nRoomMovesValidos, nLectureMovesValidos, nRoomStabilityMovesValidos,
			nMinWorkingDaysMovesValidos, nCurriculumCompactnessMovesValidos);
	printf(
			"Movimentos com Melhora:  nMoves: %7d nSwaps: %7d nKempes: %7d nTimeMoves: %7d nRoomMoves: %7d nLectureMoves: %7d nRoomStabilityMoves: %7d nMinWorkingDaysMoves: %7d nCurriculumCompactnessMoves: %7d\n",
			nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora,
			nRoomMovesMelhora, nLectureMovesMelhora, nRoomStabilityMovesMelhora,
			nMinWorkingDaysMovesMelhora, nCurriculumCompactnessMovesMelhora);
}

