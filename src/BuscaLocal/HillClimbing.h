/*
 * HillClimbing.h
 *
 *  Created on: May 26, 2015
 *      Author: erika
 */

#ifndef HILLCLIMBING_H_
#define HILLCLIMBING_H_

#include "BuscaLocal.h"
#include "../Model/Problema.h"
#include "../Vizinhancas/Movimento.h"


class HillClimbing: public BuscaLocal {
public:
	Problema* problema;
	int k;
	int tamMedioKempeChain;
	int nIteracoes;

	HillClimbing(Problema* p, int piN, int piK);
	~HillClimbing();

	Individuo* executa(Individuo* bestInd, int movimento, int nMaxMovimentosSemMelhora);
	void imprimeExecucao();
private:
	Movimento* obtemMelhorMovimento(Individuo* ind, int movimento);
};

#endif /* HILLCLIMBING_H_ */
