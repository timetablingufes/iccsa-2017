/*
 * BuscaLocal.h
 *
 *  Created on: May 26, 2015
 *      Author: erika
 */

#ifndef BUSCALOCAL_H_
#define BUSCALOCAL_H_

#include "../Model/Individuo.h"

class BuscaLocal {
public:
	int nMovimentosRealizados;
	int nIteracoesExecutadas;
	int nMoves, nSwaps, nKempes, nTimeMoves, nRoomMoves, nLectureMoves, nRoomStabilityMoves, nMinWorkingDaysMoves, nCurriculumCompactnessMoves;
	int nMovesValidos, nSwapsValidos, nKempesValidos, nTimeMovesValidos, nRoomMovesValidos, nLectureMovesValidos, nRoomStabilityMovesValidos, nMinWorkingDaysMovesValidos, nCurriculumCompactnessMovesValidos;
	int nMovesMelhora, nSwapsMelhora, nKempesMelhora, nTimeMovesMelhora, nRoomMovesMelhora, nLectureMovesMelhora, nRoomStabilityMovesMelhora, nMinWorkingDaysMovesMelhora, nCurriculumCompactnessMovesMelhora;
	int nMovesInvalido, nSwapsInvalido, nKempesInvalido, nTimeMovesInvalido, nRoomMovesInvalido, nLectureMovesInvalido, nRoomStabilityMovesInvalido, nMinWorkingDaysMovesInvalido, nCurriculumCompactnessMovesInvalido;

	BuscaLocal(){
		nMovimentosRealizados = 0;
		nIteracoesExecutadas  = 0;
		nMoves = nSwaps = nKempes = nTimeMoves = nRoomMoves = nLectureMoves = nRoomStabilityMoves = nMinWorkingDaysMoves = nCurriculumCompactnessMoves = 0;
		nMovesValidos = nSwapsValidos = nKempesValidos = nTimeMovesValidos = nRoomMovesValidos = nLectureMovesValidos = nRoomStabilityMovesValidos = nMinWorkingDaysMovesValidos = nCurriculumCompactnessMovesValidos = 0;
		nMovesMelhora = nSwapsMelhora = nKempesMelhora = nTimeMovesMelhora = nRoomMovesMelhora = nLectureMovesMelhora = nRoomStabilityMovesMelhora = nMinWorkingDaysMovesMelhora = nCurriculumCompactnessMovesMelhora = 0;
		nMovesInvalido = nSwapsInvalido = nKempesInvalido = nTimeMovesInvalido = nRoomMovesInvalido = nLectureMovesInvalido = nRoomStabilityMovesInvalido = nMinWorkingDaysMovesInvalido = nCurriculumCompactnessMovesInvalido = 0;
	};
	virtual ~BuscaLocal(){};

	virtual Individuo* executa(Individuo* ind){ return ind; };
	virtual void imprimeExecucao(){};
};

#endif /* BUSCALOCAL_H_ */
